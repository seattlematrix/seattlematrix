# SeattleMatrix

General Repo for SeattleMatrix, the organization. 

Tracks issues related to infrastructure, governance, and documentation.

Meeting notes belong in `meeting_notes` folder, please follow the existing naming pattern (i.e. SeattleMatrix_20210530.md)

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](code_of_conduct.md) 
