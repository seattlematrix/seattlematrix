# Seattle Matrix - first meeting of 2021
- https://wekan.seattlematrix.org/b/rzJSLP3Qb4TzP5Qdb/seattlematrix-first-2021-meeting

## who we are / what are we providing?
- Might be helpful to look at other services like what we’ve been aiming for:
    - https://silkky.cloud/
    - https://snopyta.org/
    - https://privacytools.io/
    - https://pixie.town/
    - https://framasoft.org/en/charte/
- want better auth before expanding too far
    - token based login works for seattle matrix now!
    - how long did it take to sign people up befoerhand?
    - not necessarily a time saver, but easier
    - will save time with increase of services/users
- also want access logs in place
    - checks for bad actors, downtime, etc
- more designated moderators/admins
    - currently: prasket, salt, ryan, dave, sntxrr
- codeified and visible CoC process
    - how to not get people pissed at rules enforcement
- think about current challenges with wekan administration, signups/access/etc
    - we are not necessarily tied to wekan
    - we want things that work with whatever SSO solution we land on
- should have graduated release of services
    - between testing (could break at any time) and "open release" (usable externally)
    - some are our services are already in daily use, like jitsi
- we also need to make sure we have a backup strategy
    - universal vs per service?
    - what "guarentees" are we offering?
    - backups vs disaster recovery?
    - matrix is currently being backed up
- re:jitsi, just use it!
    - hosted here: https://stealthyhosting.com/
    - can host your own server hardware, or just buy one of theirs
- when looking at services, SSO may be one of higher priorities, then we can find solutions that work with it
- can we get an address on the onion network?


## deployment strategy?
- https://gitlab.com/seattlematrix/seattlematrix/-/issues/21
- how do we want to deploy things? should there only be one way? is it up to the person deploying? where are things deployed? kube vs docker? do we want things to be "push button"?
- how tightly do we want to couple our solutions to the SaaS of the provider vs keeping things mobile?
    - concerned with vendor lock-in, the FLOSS path is to not rely on their infrastructure
    - we can roll our own Kube in front of 1 or more of their clusters, but then we have to manage more
    - everyone has a flavor of kube, transitioning shouldn't be a huge overhead
- start with basic principles: first goal, mid-range, long
    - first: dockerize everything, or at least use docker version, helps with portability
    - mid: centralize on our own kube vs a saas/paas vs ?
    - longer: how can we share everything we've done, and how can we make it all portable?
    - this seems important, but don't need to have explicit thoughts on how to do it yet, still important to keep in mind
- what is our mvp?
    - experimental tier, mid-level tier, graduated tier for services
    - mvp is first wave of structure we are applying, such that we can get to the point of being able to iterate
- proposal: group in charge of owning this "app"
    - experienced: sntxrr?
    - wanting to learn: cutter, tree?
- do we need kube for mvp?
    - could just have a couple of servers running a handful of docker containers
    - future step could be kube
- deployment strategy minimum:
    - from git repo, anyone should be able to build/deploy, otherwise not mvp
    - could be docker, gitlab, bash, etc. but we need to hit this mark
    - might make sense to wait for kube, if we want to intergrate automation, with kube on top
    - make it simple, anyone should be able to deploy
    - but we need to figure out $3CR37$
- is jitsi dockerized?
    - no, but it could be
    - might be an unnecessary layer
    - would be nice to be able to have it easier to stand up
- remember to keep an eye on inclusivity
    - people who want to be involved with learning together
    - this could also be beneficial for 501c3
    - document about how we built it, deployed it, checked it, etc
    - think about areas where we want to grow our own skills
    - keep work visible
    - people should shout loudly if they are disagreeing, but we need to move forward
    - if there is no voice, deployer is decider


## considerations towards centralizing for access / funding?
- all for it to live on DO, except for jitsi which needs beefier hardware
- work needs to be done to move things from sntxrr's GCP to DO
    - most is currently in kube, could just move cluster
    - would be nice to move into deployment automation that we are using going forward
    - GL has DO runners and such
- dave has already made some steps towards a DO kube cluster in DO
    - spun down at time because we weren't ready to use
    - it is all in teraform, should be easy enough to spin back up
- first step is for tech council to decide if Kube, DO, ??, etc. is the way, then we need to tie all together
    - once this is done we can establish the pattern moving forward
- how are sntxrr's services currently managed?
    - in some cases helm, some cases DIY
    - much of it was for learning, not meant to be "productized"
- pattern of abstraction for extraction is good in general
- [x] compile list of what is currently running in GCP
    - wekan, etherpad, yopass
    - started working on teleport for admin access
- currently just prasket and sntxrr who've been hacking together services
    - everything should just be owned by "seattle matrix" moving forward
    - DO is owned by org
    - prasket pays the DO bill and has access
    - org also has a PO box that prasket setup
- do we want to setup an LLC?
    - on the list to talk about
- do we want to put a freeze on new services before pattern is functioning?
    - yes, except for helper projects such as SSO
    - should still encourage experimentation, but should go through the steps being established


## 501c3 and general organizing of an organization?
- we will need org at some point
    - LL = limited liability
- Salt is considering creating an umbrella org for FLOSS stuff in the region
    - umbrella org does make some sense
    - on the face it seem reasonable
    - outside of wheelhouse of most folks here
    - as long as seattlematrix would have representation in the umbrella, sounds fine
- can always reapproach this conversation at a later date
- not having established org will hinder us when asking for funding, services, etc.
- would be nice to have donation options
    - currently they would all go on personal taxes
- reach out to other groups/projects with similar service offerings, see how they are structured
    - privacytools, etc
- [ ] salt to lead effort of getting LLC paperwork together
    - need to decide on LLC vs ... ?
    - co-op maybe?
- sntxrr knows a program/product manager, if we can employ
    - opening the door to big question about employment...
    - outside of scope for now
- we also know many people through seagl and the broader community
- getting volunteers are hard, keeping them steadyily working is hard
- as we become more senior engineers, part of our job is project management
    - also mentoring and volunteering
- prasket has been thinking about starting a mentoring project via wsealug
    - training restaurant workers with tech skills
    - sntxrr currently mentoring 2 humans


## NEXT STEPS
- [x] establish architectural council
- [ ] setup a specific chatroom for work on the platform/deployment/meta?/dogfooding
    - talk about naming in SM leadership channel
- [ ] establish processes/patterns for deployment
    - dave: best practices lead
    - cutter: architecture lead
    - iterative process, kube probably later, first priority is service consolidation
    - sntxrr: committtee lead / project manager
    - [ ] figure out how to handle secrets
- [ ] start moving services
    - [ ] prioritize SSO so that we can select services that work with our solution
- [ ] establish last sunday of the month as a standing meeting
    - should create recurring calendar invite


### future topics
- figure out the difference between WSeaLUG and SeattleMatrix
- what channels do we want on SeattleMatrix?

