# Seattle Matrix - November 2021 Meeting

attendees: salt, cutter, tree, harbinger, sntxrr, prasket

# regular meeting times?
- monthly hour after the first wsealug meeting

# prev meeting
- https://gitlab.com/seattlematrix/seattlematrix/-/blob/main/meeting_notes/SeattleMatrix_20210725.md

    - prev prev: https://gitlab.com/seattlematrix/seattlematrix/-/blob/main/meeting_notes/SeattleMatrix_20210530.md#next-steps

- infrastructure progress update: only thing left is yopass and wekan, needs mongo install
- traefik is reverse proxy that ties into docker and automagically sets things up by adding docker env vars

# secrets management
- bit warden != FLOSS, open core and requires windows db
- rust reimplementation: https://github.com/dani-garcia/vaultwarden
- easily buildable in docker: https://github.com/dani-garcia/vaultwarden/wiki/Building-your-own-docker-image
- still have a number of features in progress: https://github.com/dani-garcia/vaultwarden/issues/246
- we discussed not caring as much about human secrets, more programmatic
- vault already living on server with consul, but needs backup strategy
- snapshots can work, but there's a potential for loss if using dynamic secrets
- https://github.com/seatgeek/hashi-helper (sntxrr) will look into working with this to have better config level backups etc.
- (sntxrr) to spin up learning jams to share knowledge and build documentation, Tree/Cutter expressed interest
- (prasket) to get a redacted env var to (harbinger) 
- (harbinger) will explore teaching the ansible script for the matrix setup to speak vault
- (prasket) will explore the differences between gluu and keycloak

# open questions about project management
- prasket to step into this role

# sso
- next tech priority after secrets management
- want to decide so that we have a pattern for all new services
- https://gitlab.com/seattlematrix/seattlematrix/-/issues/13
- https://gluu.org/docs/ce/installation-guide/install-docker/
- https://gluu.org/docs/ce/installation-guide/install-kubernetes/
