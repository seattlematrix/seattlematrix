# Seattle Matrix - July 2021 Meeting

## Best Practices
- https://gitlab.com/seattlematrix/best-practices
-- repo was private, Prasket made public 
-- Salt added treedavies to Seattlematrix membership as a maintainer


### Secrets‽
- Vault vs Bitwarden
- If we are putting secrets in, let's have human do that, once entered, never has to be touched again
- https://bitwarden.com/help/api/
- https://bitwarden.com/help/article/public-api/
- https://www.vaultproject.io/docs/commands/write
- vault seems specifically focused on programatic use of secrets
- bitwarden does personal and group secret shares
- there is a Bitwarden CLI tool :)
- https://bitwarden.com/help/article/cli/
- https://github.com/envwarden/envwarden


## WSeaLUG relationship to SM 
- Add topic for next meeting to clean up rooms

## Infrastructure Progress Updates
- docker with traefik backend
- currently lots of thing tied to root, plan is to create sntxrr account and sudo out everything
- want to get ci/cd up next, that way we can share patterns without secrets
-  as soon we can get secrets store can put Matrix ansible project , maybe put it on a small node for now so others have ssh access. 


## Monthly meetings open to leadership only or all?
- open to all!
- if we need to cover Leadery things, we'll call a specific meeting for those







